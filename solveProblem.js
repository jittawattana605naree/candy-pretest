class Ball{
    constructor(store){
        this.store = store;
        this.color = ['R','G','B'];
    }
    
    couldBeColors = () =>{
        let listcolor = []
        for(let i = 0 ; i < this.store ; i++){
            var random = this.color[Math.floor(Math.random()*this.color.length)]
            listcolor.push(random)
        } 
        let close_position = this.adjacentChar(listcolor)
        return {list:listcolor,position_close:close_position}
    }

    adjacentChar = (listchar) =>{
        let count = 0;
        listchar.map((char,index) =>{
            if(char == listchar[index+1]) count++;
        })
        return count
    }
    
}

class Bank{
    constructor(amount){
        this.amount = amount;
        this.cash = [100,20,10,5,1]
    }
    return_minimum = () =>{
        let summary = this.amount;
        let count = 0;
        this.cash.map(bill =>{
            count += parseInt(summary/bill);
            summary = summary - (parseInt(summary/bill)*bill)
        })
        return count
    }
    
}

module.exports = {Ball,Bank};