const app = require('express')();
var cors = require('cors');
var morgan = require('morgan');
const bodyParser = require("body-parser");
const StoreRoutes = require('./server/routes/store.routes');
const port = 4000;
const http = require('http').Server(app);
app.use(cors());

app.use(bodyParser.urlencoded({
    extended:true
}));
app.use(bodyParser.json());
http.listen(port, () => {
	console.log('Server start in port :' + port);
});
app.use('/v1/store', StoreRoutes);

app.use(morgan('dev'));
app.use((req, res, next) => {
	const error = new Error('Not found');
	error.status = 404;
	next(error);
});
app.use((error, req, res, next) => {
	res.status(error.status || 500);
	res.json({
		error: {
			message: error.message,
		},
	});
});

const {Ball,Bank} = require('./solveProblem')

const ball = new Ball(5).couldBeColors()
const bank = new Bank(1000000000).return_minimum()
console.log(ball)
console.log(bank)
