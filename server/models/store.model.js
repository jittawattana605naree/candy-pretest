const query = require('../config/mysql');

class StoreModel{
    tablename = 'db.store';

    db_market_all_store = async() =>{
        const sql = `SELECT * FROM ${this.tablename};`
        const result = await query({sql:sql});
        return await result;
    }

    db_market_insert_store = async(name,descript,phonenumber,address,productid) =>{
        const sql = `INSERT INTO ${this.tablename}
        (namestore, descriptions, phonenumber, address)
        VALUES('${name}', '${descript}', '${phonenumber}', '${address}');`
        const result = await query({sql:sql});
        return await result;
    }
}
module.exports = new StoreModel;