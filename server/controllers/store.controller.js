const StoreModel = require('../models/store.model')

class StoreController{
    
    showAllstore = async(req,res,next) =>{
        try{
            const result  =  StoreModel.db_market_all_store()
            res.status(200).send({
                type: 'success',
                status: 200,
                message: `Show all store`,
                data: await result,
            });
        }
        catch(e){
            res.status(400).send({
				status: 400,
				message: e.message,
			});
        }
        
    }

    insertStore = async(req,res,next) =>{
        const name = req.body.name;
        const description = req.body.description;
        const phonenumber = req.body.phonenumber;
        const address = req.body.address;
        try{
            console.log(name,description,phonenumber,address)
            const result  = await StoreModel.db_market_insert_store(name,description,phonenumber,address).then(_ => {
                return {
                    type: 'success',
                    status: 200,
                    message: `successed create store`,
                }
            })
            res.status(200).send(result);
        }
        catch(e){
            res.status(400).send({
				status: 400,
				message: e.message,
			});
        }
    }

}
module.exports = new StoreController;