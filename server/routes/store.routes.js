const router = require("express").Router();
const StoreController = require('../controllers/store.controller');

router.get('/', StoreController.showAllstore);
router.post('/insertstore', StoreController.insertStore);

module.exports = router;