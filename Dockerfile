FROM node:12-alpine

WORKDIR /usr/src/app

COPY package.json .

RUN npm install
RUN apk update && \ 
    apk add --no-cache tzdata

COPY . .

CMD ["npm","start"]